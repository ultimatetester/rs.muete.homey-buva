This app adds support for the BUVA Q-Stream series.
In order for this to work, the BUVA devices needs to be connected to the same WiFi network as Homey, and the BUVA device's IP needs to be reserved in your router.
Currently, the only devices from BUVA that are smart is the Q-Stream series, hence the support for this series.

Supported Capabilities:
- Automatic Mode (internal sensor will be used to adjust fan speed)
- Boost Mode (Fan speed is boosted, overrides the automatic mode)
- Custom Fan Speed (adjust the fan speed yourself from 0-100%, overrides the automatic mode and boost mode)

Supported Flows:
- Set Automatic Mode
- Set Fan Speed

Supported Devices:
- Q-Stream 2.0

Adjusting fan speed takes a little time, this is not due to performance issues in the app or Homey, but because the BUVA device slowly ramps up the speed.
If the app does not work out for you, please report this issue on the "Report an Issue" page, and I will investigate this.