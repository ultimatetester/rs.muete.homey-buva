'use strict';

const Homey = require('homey');

class MyDriver extends Homey.Driver {
  /**
   * onInit is called when the driver is initialized.
   */
  onInit() {
    this.log('MyDriver has been initialized');
  }
}

module.exports = MyDriver;
