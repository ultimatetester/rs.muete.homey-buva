'use strict';

const Homey = require('homey');
const QStream = require('buva-qstream');

class MyDevice extends Homey.Device {
  /**
   * onInit is called when the device is initialized.
   */
  async onInit() {
    this.log('MyDevice has been initialized');

    this.data = this.getData();
    this.qstream = new QStream(this.data.ipAddress);

    this.registerCapabilityListener('onoff.automatic', this.onAutomaticModeChange.bind(this));
    this.registerCapabilityListener('onoff.boost', this.onBoostModeChange.bind(this));
    this.registerCapabilityListener('fan_speed', this.onFanSpeedChange.bind(this));

    let setAutomaticModeAction = new Homey.FlowCardAction('set_automatic_mode');
    setAutomaticModeAction.register().registerRunListener(async function(args) {
      return this.onAutomaticModeChange(args['state'] === 'true', null);
    }.bind(this));

    let setFanSpeedAction = new Homey.FlowCardAction('set_fan_speed');
    setFanSpeedAction.register().registerRunListener(async function(args) {
      return this.onFanSpeedChange(args['fan_speed'], null);
    }.bind(this));
  }

  /**
   * onAdded is called when the user adds the device, called just after pairing.
   */
  onAdded() {
    this.log('MyDevice has been added');
  }

  async onAutomaticModeChange(value) {
    console.log('onAutomaticModeChange', value);

    if (!!value) {
      await this.qstream.setTimer(0);
    } else {
      const fanSpeedValue = this.getCapabilityValue('fan_speed');
      await this.qstream.setTimer(1440, fanSpeedValue);
    }
  }

  async onBoostModeChange(value) {
    console.log('onBoostModeChange', value);

    if (!!value) {
      await this.qstream.setTimer(10, 120);
    } else {
      const automaticMode = this.getCapabilityValue('onoff.automatic');
      await this.onAutomaticModeChange(automaticMode, {});
    }
  }

  async onFanSpeedChange(value) {
    console.log('onFanSpeedChange', value);
    // TODO: Update the automatic_mode and boost_mode to be turned off when this changes!

    await this.setCapabilityValue('onoff.automatic', false);
    await this.setCapabilityValue('onoff.boost', false);

    await this.qstream.setTimer(1440, value);
  }
}

module.exports = MyDevice;
